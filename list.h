/*  this program is free software: you can redistribute it and/or modify it
 * under the terms of the gnu general public license as published by the free
 * software foundation, either version 3 of the license, or (at your option) any
 * later version.
 *
 * this program is distributed in the hope that it will be useful, but without
 * any warranty; without even the implied warranty of merchantability or fitness
 * for a particular purpose. see the gnu general public license for more
 * details.
 *
 * you should have received a copy of the gnu general public license along with
 * this program. if not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_H_
#define LIST_H_

#include <stddef.h>

struct list_node_t {
  void *data;
  struct list_node_t *next;
  struct list_node_t *previous;
};

struct list_t {
  size_t size;
  struct list_node_t *front;
  struct list_node_t *back;
};

#define foreach_list_front(node, list)                                         \
  for (struct list_node_t *node = (list)->front; node != NULL;                 \
       node = node->next)
#define foreach_list_back(node, list)                                          \
  for (struct list_node_t *node = (list)->back; node != NULL;                  \
       node = node->previous)

/* Create a new list and initialize its values. */
struct list_t *list_create();

/* Deallocate all the values stored in the linked list (including the list
 * itself) and set it to NULL. The del_function is used to deallocate complex
 * data structures. If set to NULL, a simple free() will be used instead. */
void list_destroy(struct list_t **, void (*del_function)(void *));

/* Deallocate all the contents of the linked list.
 * Different from list_destroy, this does not set the list_t* to NULL. */
void list_clear(struct list_t *, void (*del_function)(void *));

/* Link a new node to the end of the linked list structure. */
void list_push_back(struct list_t *, void *data);

/* Unlink the last node of the linked list structure, and return the value
 * inside the node. */
void *list_pop_back(struct list_t *);

/* Link a new node to the beginning of the linked list structure. */
void list_push_front(struct list_t *, void *data);

/* Unlink the first node of the linked list structure, and return the value
 * inside the node. */
void *list_pop_front(struct list_t *);

/* Create and link a new node as the next position of the list_node_t passed as
 * argument. */
void list_node_insert(struct list_t *, struct list_node_t *, void *data);

/* Remove the node passed as argument.
 * NOTE: The data stored inside the node is not deallocated. This needs to be
 * done manually. */
void list_node_remove(struct list_t *, struct list_node_t *);

#endif // LIST_H_
