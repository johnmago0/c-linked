test:
	gcc -g3 -Wall -pedantic list.c test_list.c -o test.a

clean:
	rm -rf *.a valgrind-out.txt

valgrind: test
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=valgrind-out.txt ./test.a
