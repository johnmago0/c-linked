/*  this program is free software: you can redistribute it and/or modify it
 * under the terms of the gnu general public license as published by the free
 * software foundation, either version 3 of the license, or (at your option) any
 * later version.
 *
 * this program is distributed in the hope that it will be useful, but without
 * any warranty; without even the implied warranty of merchantability or fitness
 * for a particular purpose. see the gnu general public license for more
 * details.
 *
 * you should have received a copy of the gnu general public license along with
 * this program. if not, see <https://www.gnu.org/licenses/>.
 */

/* For a better visualization of this data structure the following link is advice:
 * <https://www.cs.usfca.edu/~galles/visualization/QueueLL.html> */

#include "list.h"

#include <stdlib.h>

typedef struct list_node_t list_node_t;
typedef struct list_t list_t;

/*============================================================================*/
/*-----                             NODE                                 -----*/
/*============================================================================*/

/* Create a new node and initialize its values. */
static list_node_t *list_node_create(void *data) {
  list_node_t *node = (list_node_t *)malloc(sizeof(list_node_t));
  node->data = data;
  node->next = NULL;
  node->previous = NULL;
  return node;
}

/* Deallocate the node.
 * NOTE: This function should be called after unlinking the node. It does not
 * deallocate the data inside it. */
static void list_node_destroy(list_node_t **node) {
  (*node)->next = NULL;
  (*node)->previous = NULL;
  free(*node);
  *node = NULL;
}

/*============================================================================*/
/*-----                             LIST                                 -----*/
/*============================================================================*/

list_t *list_create() {
  list_t *list = (list_t *)malloc(sizeof(list_t));
  list->size = 0;
  list->front = NULL;
  list->back = NULL;
  return list;
}

void list_destroy(list_t **list, void (*del_function)(void *)) {
  list_clear(*list, del_function);
  free(*list);
  *list = NULL;
}

void list_clear(list_t *list, void (*del_function)(void *)) {
  while (list->size != 0) {
    void *data = list_pop_back(list);
    if (del_function == NULL)
      free(data);
    else {
      del_function(data);
      free(data);
    }
  }
}

void list_push_back(list_t *list, void *data) {
  if (list->size == 0) {
    list->back = list_node_create(data);
    list->front = list->back;
    list->size++;
    return;
  }

  list_node_t *node = list_node_create(data);
  node->previous = list->back;
  list->back->next = node;
  list->back = node;
  list->size++;
}

void *list_pop_back(list_t *list) {
  if (list->size == 0)
    return NULL;

  void *data;
  if (list->back == list->front) {
    data = list->back->data;
    list_node_destroy(&list->back);
    list->front = NULL;
    list->size--;
    return data;
  }

  data = list->back->data;
  list_node_t *node = list->back;
  list->back = list->back->previous;
  list->back->next = NULL;
  list_node_destroy(&node);
  list->size--;
  return data;
}

void list_push_front(list_t *list, void *data) {
  if (list->size == 0) {
    list->front = list_node_create(data);
    list->back = list->front;
    list->size++;
    return;
  }

  list_node_t *node = list_node_create(data);
  node->next = list->front;
  list->front->previous = node;
  list->front = node;
  list->size++;
}

void *list_pop_front(list_t *list) {
  if (list->size == 0)
    return NULL;

  void *data;
  if (list->front == list->back) {
    data = list->front->data;
    list_node_destroy(&list->front);
    list->back = NULL;
    list->size--;
    return data;
  }

  data = list->front->data;
  list_node_t *node = list->front;
  list->front = list->front->next;
  list->front->previous = NULL;
  list_node_destroy(&node);
  list->size--;
  return data;
}

void list_node_insert(list_t *list, list_node_t *node, void *data) {
  if (list->size == 0 || list->back == node) {
    list_push_back(list, data);
    return;
  }

  list_node_t *newNode = list_node_create(data);
  newNode->next = node->next;
  newNode->previous = node;
  node->next->previous = newNode;
  node->next = newNode;
  list->size++;
}

void list_node_remove(list_t *list, list_node_t *node) {
  if (list->front == node) {
    list_pop_front(list);
    return;
  }

  if (list->back == node) {
    list_pop_back(list);
    return;
  }

  list_node_t *nodeNext = node->next;
  list_node_t *nodePrevious = node->previous;
  nodeNext->previous = nodePrevious;
  nodePrevious->next = nodeNext;
  list_node_destroy(&node);
  list->size--;
}
