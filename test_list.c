#include "list.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define NTESTS 6
#define ARRAY_SIZE 10
#define LOOP_COUNT 10

int *array_create(int size) {
  int *array = (int *)malloc(sizeof(int) * size);

  for (int i = 0; i < size; i++)
    array[i] = i;

  return array;
}

/*----------------------------------------------------------------------------*/

bool test_push_back() {
  int *array = array_create(ARRAY_SIZE);
  struct list_t *list = list_create();

  int i;
  for (i = 0; i < ARRAY_SIZE; i++) {
    int *j = (int *)malloc(sizeof(int));
    *j = array[i];
    list_push_back(list, j);
  }

  i = 0;
  foreach_list_front(node, list) {
    if (*(int *)node->data != array[i]) {
      printf("Inserted value does not match array: array[%d](%d) != node(%d)\n",
             i, array[i], *(int *)node->data);
      list_destroy(&list, NULL);
      free(array);
      return false;
    }
    i++;
  }

  if (list->size != ARRAY_SIZE) {
    printf("List size differente from array size: %d != %d\n", (int)list->size,
           ARRAY_SIZE);
    list_destroy(&list, NULL);
    free(array);
    return false;
  }

  list_destroy(&list, NULL);
  free(array);
  return true;
}

bool test_push_front() {
  int *array = array_create(ARRAY_SIZE);
  struct list_t *list = list_create();

  int i;
  for (i = 0; i < ARRAY_SIZE; i++) {
    int *j = (int *)malloc(sizeof(int));
    *j = array[i];
    list_push_front(list, j);
  }

  i = 0;
  foreach_list_back(node, list) {
    if (*(int *)node->data != array[i]) {
      printf("Inserted value does not match array: array[%d](%d) != node(%d)\n",
             i, array[i], *(int *)node->data);
      list_destroy(&list, NULL);
      free(array);
      return false;
    }
    i++;
  }

  if (list->size != ARRAY_SIZE) {
    printf("List size differente from array size: %d != %d\n", (int)list->size,
           ARRAY_SIZE);
    list_destroy(&list, NULL);
    free(array);
    return false;
  }

  list_destroy(&list, NULL);
  free(array);
  return true;
}

bool test_pop_back() {
  int *array = array_create(ARRAY_SIZE);
  struct list_t *list = list_create();

  for (int i = 0; i < ARRAY_SIZE; i++) {
    int *j = (int *)malloc(sizeof(int));
    *j = array[i];
    list_push_back(list, j);
  }

  for (int i = ARRAY_SIZE - 1; i >= 0; i--) {
    int *data = list_pop_back(list);
    if (*data != array[i]) {
      printf("Inserted value does not match array: array[%d](%d) != node(%d)\n",
             i, array[i], *data);
      list_destroy(&list, NULL);
      free(array);
      return false;
    }
    free(data);
  }

  if (list->size != 0) {
    printf("List size differente from zero: %d\n", (int)list->size);
    list_destroy(&list, NULL);
    free(array);
    return false;
  }

  list_destroy(&list, NULL);
  free(array);
  return true;
}

bool test_pop_front() {
  int *array = array_create(ARRAY_SIZE);
  struct list_t *list = list_create();

  for (int i = 0; i < ARRAY_SIZE; i++) {
    int *j = (int *)malloc(sizeof(int));
    *j = array[i];
    list_push_back(list, j);
  }

  for (int i = 0; i < ARRAY_SIZE; i++) {
    int *data = list_pop_front(list);
    if (*data != array[i]) {
      printf("Inserted value does not match array: array[%d](%d) != node(%d)\n",
             i, array[i], *data);
      list_destroy(&list, NULL);
      free(array);
      return false;
    }
    free(data);
  }

  if (list->size != 0) {
    printf("List size differente from zero: %d\n", (int)list->size);
    list_destroy(&list, NULL);
    free(array);
    return false;
  }

  list_destroy(&list, NULL);
  free(array);
  return true;
}

bool test_insert() {
  int *array = array_create(ARRAY_SIZE);
  struct list_t *list = list_create();

  for (int i = 0; i < ARRAY_SIZE; i++) {
    int *j = (int *)malloc(sizeof(int));
    *j = array[i];
    list_push_back(list, j);
  }

  for (int i = 0, j = 0; i < LOOP_COUNT; i++) {
    int irand = rand() % (ARRAY_SIZE + i);
    int *data = (int *)malloc(sizeof(int));
    *data = irand;
    foreach_list_front(node, list) {
      if (j == irand + 1) {
        if (*(int *)node->data != irand) {
          printf("Value inserted differ from the actual value: val(%d) != "
                 "node(%d)",
                 irand, *(int *)node->data);
          list_destroy(&list, NULL);
          free(array);
          return false;
        }
      }

      if (j == irand)
        list_node_insert(list, node, data);
      j++;
    }
    j = 0;
  }

  if (list->size != ARRAY_SIZE + LOOP_COUNT) {
    printf("Value not inserted in list\n");
    list_destroy(&list, NULL);
    free(array);
    return false;
  }

  list_destroy(&list, NULL);
  free(array);
  return true;
}

bool test_remove() {
  int *array = array_create(ARRAY_SIZE + LOOP_COUNT);
  struct list_t *list = list_create();

  for (int i = 0; i < ARRAY_SIZE + LOOP_COUNT; i++) {
    int *j = (int *)malloc(sizeof(int));
    *j = array[i];
    list_push_back(list, j);
  }

  for (int i = 0, j = 0; i < LOOP_COUNT; i++) {
    int irand = rand() % (ARRAY_SIZE + LOOP_COUNT - i);
    foreach_list_front(node, list) {
      if (j == irand) {
        free(node->data);
        node = node->next;
        list_node_remove(list, node->previous);
      }
      j++;
    }
    j = 0;
  }

  if (list->size != ARRAY_SIZE) {
    printf("Value not inserted in list\n");
    list_destroy(&list, NULL);
    free(array);
    return false;
  }

  list_destroy(&list, NULL);
  free(array);
  return true;
}

int main() {
  int errs = NTESTS;
  if (!test_push_back()) {
    printf("Push back did not work\n");
    errs--;
  }

  if (!test_push_front()) {
    printf("Push front did not work\n");
    errs--;
  }

  if (!test_pop_back()) {
    printf("Pop back did not work\n");
    errs--;
  }

  if (!test_pop_front()) {
    printf("Pop front did not work\n");
    errs--;
  }

  if (!test_insert()) {
    printf("Insert did not work\n");
    errs--;
  }

  if (!test_remove()) {
    printf("Remove did not work\n");
    errs--;
  }

  printf("Tests %d/%d passed\n", errs, NTESTS);
}